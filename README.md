# scripts
Scripts that I use often to make my life easier... 
I plan to update this more frequently and will try to keep it as simple as possible... Please send me a pull request if you have some improvements or drop a line in the issues section if you need something to be fixed.

1) docker2ip: This is a simple script that can be used to extract an ip address from a docker container. you can run it as docker2ip containerID (this assumes docker2ip can be found via $PATH, else prefix ./ to the command)
